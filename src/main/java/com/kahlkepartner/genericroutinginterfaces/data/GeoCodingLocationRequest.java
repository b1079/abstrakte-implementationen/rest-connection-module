package com.kahlkepartner.genericroutinginterfaces.data;

import lombok.Data;
import model.geometry.GeoJSONPoint;

@Data
public class GeoCodingLocationRequest {
    double lat;
    double lon;
    String api;
}
