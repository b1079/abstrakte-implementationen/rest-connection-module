package com.kahlkepartner.genericroutinginterfaces.data;

import interfaces.data.Address;
import lombok.Data;

@Data
public class GeoCodingAdressRequest {
    Address location;
    String api;
}
