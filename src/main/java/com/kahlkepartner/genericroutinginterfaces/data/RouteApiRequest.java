package com.kahlkepartner.genericroutinginterfaces.data;

import interfaces.data.Address;
import lombok.Data;

@Data
public class RouteApiRequest {
    Address from;
    Address to;
    String api;
}
